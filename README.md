# Digital State #

Welcome To Digital State Project :)

This projet is developed with MultiChain 1.0 and MultichainClient.NET (Credits for SolarLabRU)

Note: For run this project you need download and start MultiChain 1.0 or above

Please read this page for more info = https://www.multichain.com/getting-started/

#### 1. How to connect to MultiChain: ####
* Add this function for connect:

		private MultiChainClient client = new MultiChainClient(IP, RCP-Port, SSL, Username, Password, Hostname);
        
* How to get data of connection:

        1. IP = View in MultiChain console (If the node is in your PC you must write "LocalHost")

        2. RCP-PORT = Search the file "%AppData%/Roaming/MultiChain/"You_Node_Name"/Params.Dat" Or View in MultiChain Console (Port-1)
        
        3. SSL = You can write "true" or "False" Here

        4. Username = Search the file "%AppData%/Roaming/MultiChain/"You_Node_Name"/Multichain.conf"

        5. Password = Search the file "%AppData%/Roaming/MultiChain/"You_Node_Name"/Multichain.conf"

        6. Hostname = Search the file "%AppData%/Roaming/MultiChain/"You_Node_Name"/Params.Dat" Or View in MultiChain Console

#### 2. How to save data in MultiChain: ####

* Create New Stream

		var create_stream = await client.Create("stream", rfid, false);
            
* Publish Data In Stream

     	var publish_data = await client.Publish(rfid, cedula, MyJson);
	
* Subscribe To Stream

		var subscribed_stream = await client.Subscribe(rfid);

#### 3. How to view data of MultiChain: ####

* View All Streams Data (Subscribed Necessary)

		var info = await client.ListStreams();
        
     	foreach (var item1 in info.Result)
     	{
        	if (item1.Name != "root") {
       			if (item1.Subscribed == true){
                        var datos = await client.ListStreamItems(item1.Name);
                        foreach (var item2 in datos.Result)
                        {
                            Persona identidad = new Persona();
                            identidad.ID = item1.Name;
                            identidad.Cedula = item2.Key;
                            identidad.Datos = HexStringToString(item2.Data);
                        }
                    }
                }
            }
        
* View Data Of One Person With RFID (Subscribed Necessary)

		var datos = await client.ListStreamItems(rfid);
        
        foreach (var item in datos.Result)
        {
          identidad.ID = rfid;
          identidad.Cedula = item.Key;
          identidad.Datos = HexStringToString(item.Data);
        }
        
* Note: The Data received of Multichain is encripted in a String Whith base64 code. For desencripted the data you need to use this function:

		 public string HexStringToString(string hexString)
        {
            if (hexString == null || (hexString.Length & 1) == 1)
            {
                throw new ArgumentException();
            }
            var sb = new StringBuilder();
            for (var i = 0; i < hexString.Length; i += 2)
            {
                var hexChar = hexString.Substring(i, 2);
                sb.Append((char)Convert.ToByte(hexChar, 16));
            }
            return sb.ToString();
        }