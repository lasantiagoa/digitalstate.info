﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DigitalState.TechDay.Models;
using Platform.DataAccess.MultiChain.Client;
using Newtonsoft.Json;
using System.Collections;

namespace DigitalState.TechDay.Controllers
{
    public class HomeController : Controller
    {

        /* Connect to the MultiChain  */
        private MultiChainClient client = new MultiChainClient("localhost", 4404, false, "multichainrpc", "12345678", "DigitalState");

        /*  
        How To Get Connetion Data:
        1. IP = MultiChain Console (If The Node Is In Your PC You Must Write "LocalHost")
        2. RCP-PORT = %AppData%/Roaming/MultiChain/"NodeName"/Params.Dat Or MultiChain Console Port -1
        3. Username = %AppData%/Roaming/MultiChain/"NodeName"/Multichain.conf
        4. Password = %AppData%/Roaming/MultiChain/"NodeName"/Multichain.conf
        5. Hostname = %AppData%/Roaming/MultiChain/"NodeName"/Params.Dat Or MultiChain Console
        */

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Apply()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<ActionResult> SaveData(string rfid, string cedula, string nombre, string apellido, string sexo, string nacionalidad, string fecha_nacimiento, string correo)
        {
            /* Create JSON For Data */
            Datos data = new Datos
            {
                Nombre = nombre,
                Apellido = apellido,
                Sexo = sexo,
                Nacionalidad = nacionalidad,
                Fecha_Nacimiento = fecha_nacimiento,
                Correo = correo
            };
            string MyJson = JsonConvert.SerializeObject(data);

            /* Create New Stream */
            var create_stream = await client.Create("stream", rfid, false);
            
            /* Publish Data In Stream */
            var publish_data = await client.Publish(rfid, cedula, MyJson);

            /* Subscribe To Stream */
            var subscribed_stream = await client.Subscribe(rfid);

            /* Return Message*/
            return Json(new { msg = "Data Saved Successfully" });
        }

        [HttpPost]
        public async Task<ActionResult> ViewPersons()
        {
            /* Create Object For Person */
            Stack myStack = new Stack();

            /* View All Streams Data (Subscribed Necessary) */
            var info = await client.ListStreams();
            foreach (var item1 in info.Result)
            {
                if (item1.Name != "root") {
                    if (item1.Subscribed == true)
                    {
                        var datos = await client.ListStreamItems(item1.Name);
                        foreach (var item2 in datos.Result)
                        {
                            Persona identidad = new Persona();
                            identidad.ID = item1.Name;
                            identidad.Cedula = item2.Key;
                            identidad.Datos = HexStringToString(item2.Data);
                            myStack.Push(identidad);
                        }
                    }
                }
            }
            return Json(new { personas = myStack });
        }

        [HttpPost]
        public async Task<ActionResult> ViewPersonData(string rfid)
        {
            /* Create Object For Person */
            Persona identidad = new Persona();

            /* View All Streams Data (Subscribed Necessary) */
            var datos = await client.ListStreamItems(rfid);
            foreach (var item in datos.Result)
            {
                identidad.ID = rfid;
                identidad.Cedula = item.Key;
                identidad.Datos = HexStringToString(item.Data);
            }
            return Json(new { cedula = identidad.Cedula, datos = identidad.Datos });
        }

        /* Function For Convert Hex-Data To String */
        public string HexStringToString(string hexString)
        {
            if (hexString == null || (hexString.Length & 1) == 1)
            {
                throw new ArgumentException();
            }
            var sb = new StringBuilder();
            for (var i = 0; i < hexString.Length; i += 2)
            {
                var hexChar = hexString.Substring(i, 2);
                sb.Append((char)Convert.ToByte(hexChar, 16));
            }
            return sb.ToString();
        }
    }
}
