﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DigitalState.TechDay.Models
{
    public class Persona
    {
        public string ID { get; set; }
        public string Cedula { get; set; }
        public string Datos { get; set; }
    }
}
